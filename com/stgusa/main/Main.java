/**
 * author: Raymond Mintz, database view: Jeff Nikodym
 * rmintz@stgusa.com
 * jnikodym@stgusa.com
 * 
 * date 10-31-2014 
 * 
 * Purpose:
 * 
 * 
 *
 * This will only email 2720, 2150, 1578, and 
 */
package com.stgusa.main;

import com.stgusa.rajparadereport.email.CustomerEmail;

public class Main {

	public static void main(String[] args) throws InterruptedException {

		DBQuery dbq = new DBQuery();
		dbq.Connection();

		String test = "rbmintz.linuxadm@gmail.com";
		String[] email2720 = { "planning@sunham.com", "faddam@stgusa.com" }; // test
																					// will
																					// be
																					// taken
																					// out
																					// deleted
																					// after
																					// testing
																					// period
		String[] email2150 = { "jmonge@usluggage.com", "maribelf@stgusa.com" };
		String[] email2703 = { "oscarc@bioworldmerch.com",
				"anb@bioworldmerch.com", "promero@stgusa.com"};
		String[] email1578 = { "nthomas@dynamicbrands.com",
				"promero@stgusa.com"};
		String[] email2243 = { "promero@stgusa.com"};
		String[] email2714 = { "acastanon@stgusa.com"};
		String[] email2687 = { "promero@stgusa.com"};

		Compress comp = new Compress("/mnt/synapse/ftproot/2720/shipped.csv",
				"/mnt/synapse/ftproot/2720/shipped.csv.gz");
		comp.compressIt();

		CustomerEmail ce1578 = new CustomerEmail("1578", email1578,
				dbq.getPrefix());
		ce1578.sendMail();
		Thread.sleep(100);

		CustomerEmail ce2703 = new CustomerEmail("2703", email2703,
				dbq.getPrefix());
		ce2703.sendMail();
		Thread.sleep(100);

		CustomerEmail ce2150 = new CustomerEmail("2150", email2150,
				dbq.getPrefix());
		ce2150.sendMail();
		Thread.sleep(100);

		CustomerEmail ce2720 = new CustomerEmail("2720", email2720,
				dbq.getPrefix());
		ce2720.sendMail();
		Thread.sleep(100);

		CustomerEmail ce2243 = new CustomerEmail("2243", email2243,
				dbq.getPrefix());
		ce2243.sendMail();
		Thread.sleep(100);

		CustomerEmail ce2714 = new CustomerEmail("2714", email2714,
				dbq.getPrefix());
		ce2714.sendMail();
		Thread.sleep(100);

		CustomerEmail ce2687 = new CustomerEmail("2687", email2687,
				dbq.getPrefix());
		ce2687.sendMail();

	}
}

package com.stgusa.main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

public class Compress {
	String fileName;
	String toZip;

	public Compress(String fileName, String toZip) {
		this.fileName = fileName;
		this.toZip = toZip;
	}

	public void compressIt() {

		int myByte;
		try {
			FileInputStream fileIn = new FileInputStream(fileName);
			GZIPOutputStream gzOut = new GZIPOutputStream(new FileOutputStream(
					toZip));

			while ((myByte = fileIn.read()) != -1) {
				gzOut.write(myByte);
				System.out.println(myByte);
			}

			gzOut.close();
			fileIn.close();

			System.out.println("DONE>>>>");

		} catch (FileNotFoundException e) {
			System.out.println("FAILED>>> " + e.getMessage());
		} catch (IOException e) {
			System.out.println("FAILED>>> " + e.getMessage());
			e.printStackTrace();
		}

	}

}

package com.stgusa.main;

/**
 * author: Raymond Mintz, database view: Jeff Nikodym
 * date 10-31-2014 
 * 
 * Purpose:
 * 
 * 
 */

//Status

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;

public class DBQuery {

	private String query = "select * from myView where ORDER_ENTRY > sysdate - 90";
	private String username = "usernameHere";
	private String password = "passwordHere";
	private String ip = "myIP";
	private String database = "myDatabase";
	final String url = "jdbc:oracle:thin:@//"+ip+"/"+database;
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	// Header titles
	String STATUS, MODE, STG_LOADNO, CUSTID, RETAILER, BILL_TO, PRO_NUMBER, PO,
			VICS_BOL, STATUS_NAME, MODE_NAME, CUST_REF, RETAILER_NAME, CONID, EXTERNAL, ROUTING_CONF;

	Date APPT_DATE, ORDER_ENTRY, SHIPDATE, CANCEL_DATE;
	Time TIME_IN, TIME_OUT, APPT_TIME;

	int WEIGHT, CUBE, CASES, PEICES;

	java.sql.Date DATE_OUT;

	public DBQuery(String customer) {
		query = "select * from STG_PICKUP_REPORT where custid = \'" + customer
				+ "\'";
	}

	// File Directory here

	public String prefix = "/mnt/synapse/ftproot/";
	String s = "shipped.csv";
	String o = "open.csv";

	File _1578s = new File(prefix + "1578/" + s);
	File _1578o = new File(prefix + "1578/" + o);
	File _1724s = new File(prefix + "1724/" + s);
	File _1724o = new File(prefix + "1724/" + o);
	File _2149s = new File(prefix + "2149/" + s);
	File _2149o = new File(prefix + "2149/" + o);
	File _2150s = new File(prefix + "2150/" + s);
	File _2150o = new File(prefix + "2150/" + o);
	File _2243s = new File(prefix + "2243/" + s);
	File _2243o = new File(prefix + "2243/" + o);
	File _2369s = new File(prefix + "2369/" + o);
	File _2369o = new File(prefix + "2369/" + s);
	File _2684s = new File(prefix + "2684/" + s);
	File _2684o = new File(prefix + "2684/" + o);
	File _2687s = new File(prefix + "2687/" + s);
	File _2687o = new File(prefix + "2687/" + o);
	File _2691s = new File(prefix + "2691/" + s);
	File _2691o = new File(prefix + "2691/" + o);
	File _2697s = new File(prefix + "2697/" + s);
	File _2697o = new File(prefix + "2697/" + o);
	File _2703s = new File(prefix + "2703/" + s);
	File _2703o = new File(prefix + "2703/" + o);
	File _2713s = new File(prefix + "2713/" + s);
	File _2713o = new File(prefix + "2713/" + o);
	File _2714s = new File(prefix + "2714/" + s);
	File _2714o = new File(prefix + "2714/" + o);
	File _2715s = new File(prefix + "2715/" + s);
	File _2715o = new File(prefix + "2715/" + o);
	File _2720s = new File(prefix + "2720/" + s);
	File _2720o = new File(prefix + "2720/" + o);
	File _2721s = new File(prefix + "2721/" + s);
	File _2721o = new File(prefix + "2721/" + o);
	File _3666s = new File(prefix + "3666/" + s);
	File _3666o = new File(prefix + "3666/" + o);

	public ArrayList<File> fileList = new ArrayList<>();

	// "select * from STG_PICKUP_REPORT where custid = '2720' and LOADSTATUS <> '9'";
	// String query = "select * from STG_PICKUP_REPORT where custid = '2720'";

	public DBQuery() {
		this.username = username;
		this.password = password;

	}

	public void Connection() {

		fileList.add(_1578s);
		fileList.add(_1578o);
		fileList.add(_1724s);
		fileList.add(_1724o);
		fileList.add(_2149s);
		fileList.add(_2149o);
		fileList.add(_2150s);
		fileList.add(_2150o);
		fileList.add(_2243s);
		fileList.add(_2243o);
		fileList.add(_2369s);

		fileList.add(_2369o);
		fileList.add(_2684s);
		fileList.add(_2684o);
		fileList.add(_2687s);
		fileList.add(_2687o);
		fileList.add(_2691s);
		fileList.add(_2691o);
		fileList.add(_2697s);
		fileList.add(_2697o);
		fileList.add(_2703s);
		fileList.add(_2703o);

		fileList.add(_2713s);
		fileList.add(_2713o);
		fileList.add(_2714s);
		fileList.add(_2714o);
		fileList.add(_2715s);
		fileList.add(_2715o);
		fileList.add(_2720s);
		fileList.add(_2720o);
		fileList.add(_2721s);
		fileList.add(_2721o);
		fileList.add(_3666s);
		fileList.add(_3666o);

		// Clear files
		writeHeader();

		// CHECK FOR FILE AND CREATE IF NOT EXISTS

		// BEGIN DATABASE QUERY AND STING ASSIGNMENT
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.out.println("CANT FIND DRIVER: "
					+ "oracle.jdbc.driver.OracleDriver");
			e.printStackTrace();
		}

		//
		try (FileWriter _2720fws = new FileWriter(_2720s, true);
				FileWriter _2720fwo = new FileWriter(_2720o, true);
				FileWriter _1578fws = new FileWriter(_1578s, true);
				FileWriter _1578fwo = new FileWriter(_1578o, true);
				FileWriter _1724fws = new FileWriter(_1724s, true);
				FileWriter _1724fwo = new FileWriter(_1724o, true);
				FileWriter _2149fws = new FileWriter(_2149s, true);
				FileWriter _2149fwo = new FileWriter(_2149o, true);
				FileWriter _2150fws = new FileWriter(_2150s, true);
				FileWriter _2150fwo = new FileWriter(_2150o, true);
				FileWriter _2243fws = new FileWriter(_2243s, true);
				FileWriter _2243fwo = new FileWriter(_2243o, true);
				FileWriter _2369fws = new FileWriter(_2369s, true);
				FileWriter _2369fwo = new FileWriter(_2369o, true);
				FileWriter _2684fws = new FileWriter(_2684s, true);
				FileWriter _2684fwo = new FileWriter(_2684o, true);

				FileWriter _2687fws = new FileWriter(_2687s, true);
				FileWriter _2687fwo = new FileWriter(_2687o, true);
				FileWriter _2691fws = new FileWriter(_2691s, true);
				FileWriter _2691fwo = new FileWriter(_2691o, true);
				FileWriter _2697fws = new FileWriter(_2697s, true);
				FileWriter _2697fwo = new FileWriter(_2697o, true);
				FileWriter _2703fws = new FileWriter(_2703s, true);
				FileWriter _2703fwo = new FileWriter(_2703o, true);
				FileWriter _2713fws = new FileWriter(_2713s, true);
				FileWriter _2713fwo = new FileWriter(_2713o, true);
				FileWriter _2714fws = new FileWriter(_2714s, true);
				FileWriter _2714fwo = new FileWriter(_2714o, true);
				FileWriter _2715fws = new FileWriter(_2715s, true);
				FileWriter _2715fwo = new FileWriter(_2715o, true);
				FileWriter _2721fws = new FileWriter(_2721s, true);
				FileWriter _2721fwo = new FileWriter(_2721o, true);
				FileWriter _3666fws = new FileWriter(_3666s, true);
				FileWriter _3666fwo = new FileWriter(_3666o, true);) {

			String[] token;

			conn = DriverManager.getConnection(url, username, password);
			stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			rs = stmt.executeQuery(query);

			System.out.println("Writing to file...");

			// int line = 0; // LIMIT
			// int limit = 4000; // LIMIT

			while (rs.next()) {
				STATUS = rs.getString("LOADSTATUS");

				SHIPDATE = rs.getDate("STATUSUPDATE");
				CUSTID = rs.getString("CUSTID");
				MODE = rs.getString("SHIPTYPE");
				STG_LOADNO = rs.getString("STG_LOADNO");
				CONID = rs.getString("CONID");
				RETAILER = rs.getString("RETAILER");

				PRO_NUMBER = rs.getString("PRO_NUMBER");
				BILL_TO = rs.getString("BILL_TO");
				PO = rs.getString("PO");
				CASES = rs.getInt("CASES");
				WEIGHT = rs.getInt("WEIGHTORDER");
				CUBE = rs.getInt("CUBEORDER");
				VICS_BOL = rs.getString("VIC_BOL");
				TIME_IN = rs.getTime("TIME_IN");
				TIME_OUT = rs.getTime("TIME_OUT");
				APPT_TIME = rs.getTime("APPT_TIME");
				APPT_DATE = rs.getDate("APPT_TIME");
				DATE_OUT = rs.getDate("TIME_OUT");
				CUST_REF = rs.getString("CUST_REF");
				RETAILER_NAME = rs.getString("RETAILER_NAME");
				// CARRIER_NAME = rs.getString("CARRIER_NAME");
				ORDER_ENTRY = rs.getDate("ORDER_ENTRY");
				CANCEL_DATE = rs.getDate("CANCEL_DATE");
				PEICES = rs.getInt("EACHES");
				EXTERNAL = rs.getString("EXTERNAL");
				ROUTING_CONF = rs.getString("ROUTING_CONF");

				switch (MODE) {
				case "L":
					MODE_NAME = "LTL";
					break;
				case "S":
					MODE_NAME = "SMP";
					break;
				case "T":
					MODE_NAME = "TRUCK";
					break;
				case "P":
					MODE_NAME = "CUSTOMER PICK-UP";
					break;
				default:
					MODE_NAME = "SHIPTYPE: " + MODE + " NOT FOUND";
				}

				switch (STATUS) {
				case "0":
					STATUS_NAME = "HOLD";
					break;
				case "1":
					STATUS_NAME = "ENTERED";
					break;
				case "2":
					STATUS_NAME = "COMMITTED";
					break;
				case "3":
					STATUS_NAME = "PART RELEASE";
					break;
				case "4":
					STATUS_NAME = "RELEASED";
					break;

				case "5":
					STATUS_NAME = "PICKING IN PROGRESS";
					break;
				case "6":
					STATUS_NAME = "PICKED";
					break;
				case "7":
					STATUS_NAME = "LOADING";
					break;
				case "8":
					STATUS_NAME = "LOADED";
					break;
				case "9":
					STATUS_NAME = "SHIPPED";
					break;
				case "X":
					STATUS_NAME = "CANCELLED";
				default:
					STATUS_NAME = "UNKOWN STATUS NUMBER";
				}

				// BILL_TO needs to be in quotes to keep the comma from
				// seperating the column
				String format = STATUS_NAME + "," + CUSTID + "," + CUST_REF
						+ "," + MODE_NAME + "," + STG_LOADNO + "," + CONID
						+ "," + "\"" + RETAILER + "\"" + "," + "\""
						+ RETAILER_NAME + "\"" + "," + "\"" + BILL_TO + "\""
						+ "," + PO + "," + PEICES + "," + CASES + "," + WEIGHT
						+ "," + CUBE + "," +"\'"+ VICS_BOL +"\'"+ "," + PRO_NUMBER + ","
						+ ORDER_ENTRY + "," + CANCEL_DATE + "," + APPT_DATE
						+ "," + SHIPDATE + "," + APPT_TIME + "," + TIME_IN
						+ "," + TIME_OUT +"," +EXTERNAL+ ","+ROUTING_CONF;
				System.out.println(format);

				String queryResult = format + "\n";

				token = queryResult.split(",");

				if (token[1].matches("1578"))
					if (token[0].matches("SHIPPED"))
						_1578fws.write(queryResult);
					else
						_1578fwo.write(queryResult);

				if (token[1].matches("1724"))
					if (token[0].matches("SHIPPED"))
						_1724fws.write(queryResult);
					else
						_1724fwo.write(queryResult);

				if (token[1].matches("2149"))
					if (token[0].matches("SHIPPED"))
						_2149fws.write(queryResult);
					else
						_2149fwo.write(queryResult);

				if (token[1].matches("2150"))
					if (token[0].matches("SHIPPED"))
						_2150fws.write(queryResult);
					else
						_2150fwo.write(queryResult);

				if (token[1].matches("2243"))
					if (token[0].matches("SHIPPED"))
						_2243fws.write(queryResult);
					else
						_2243fwo.write(queryResult);

				if (token[1].matches("2369"))
					if (token[0].matches("SHIPPED"))
						_2369fws.write(queryResult);
					else
						_2369fwo.write(queryResult);

				if (token[1].matches("2684"))
					if (token[0].matches("SHIPPED"))
						_2684fws.write(queryResult);
					else
						_2684fwo.write(queryResult);

				if (token[1].matches("2687"))
					if (token[0].matches("SHIPPED"))
						_2687fws.write(queryResult);
					else
						_2687fwo.write(queryResult);

				if (token[1].matches("2691"))
					if (token[0].matches("SHIPPED"))
						_2691fws.write(queryResult);
					else
						_2691fwo.write(queryResult);

				if (token[1].matches("2697"))
					if (token[0].matches("SHIPPED"))
						_2697fws.write(queryResult);
					else
						_2697fwo.write(queryResult);

				if (token[1].matches("2703"))
					if (token[0].matches("SHIPPED"))
						_2703fws.write(queryResult);
					else
						_2703fwo.write(queryResult);

				if (token[1].matches("2713"))
					if (token[0].matches("SHIPPED"))
						_2713fws.write(queryResult);
					else
						_2713fwo.write(queryResult);

				if (token[1].matches("2714"))
					if (token[0].matches("SHIPPED"))
						_2714fws.write(queryResult);
					else
						_2714fwo.write(queryResult);

				if (token[1].matches("2715"))
					if (token[0].matches("SHIPPED"))
						_2715fws.write(queryResult);
					else
						_2715fwo.write(queryResult);

				if (token[1].matches("2720"))
					if (token[0].matches("SHIPPED"))
						_2720fws.write(queryResult);
					else
						_2720fwo.write(queryResult);

				if (token[1].matches("2721"))
					if (token[0].matches("SHIPPED"))
						_2721fws.write(queryResult);
					else
						_2721fwo.write(queryResult);

				if (token[1].matches("3666"))
					if (token[0].matches("SHIPPED"))
						_3666fws.write(queryResult);
					else
						_3666fwo.write(queryResult);

				// line += 1; // LIMIT

			}

			rs.close();

		} catch (SQLException e) {
			System.out.println("issue with SQL: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("CANT WRITE TO FILE: " + e.getMessage());
		}

	}

	public String getPrefix() {
		return prefix;
	}

	
	/*
	 * RETAILER HAS been changed to LOAD_AUTH
	 * the DATABASE FIELDS ARE STILL CALLED RETAILER
	 */
	public void writeHeader() {
		String header = "STATUS_NAME , CUSTID , CUST_REF ,"
				+ "MODE_NAME , STG_LOADNO ,CONID, LOAD_AUTH ,"
				+ " RETAILER_NAME , BILL_TO, PO , PIECES,CASES , WEIGHT"
				+ ", CUBE , VICS_BOL,PRO_NUMBER, ORDER_ENTRY, CANCEL_DATE, APPT_DATE ,"
				+ " SHIP_DATE , APPT_TIME , TIME_IN ,TIME_OUT,EXTERNAL\\ROUTING_NUMBER,ROUTING_CONF" +"\n";

		for (File files : fileList) {
			System.out.println(files);
			try {
				files.createNewFile();
				System.out.println(String.format("%s created\n",
						files.toString()));
			} catch (IOException e1) {
				System.out.println("COULD NOT CREATE FILE:\t" + files);
			}
			try {
				FileWriter temp = new FileWriter(files);
				temp.write(header);
				temp.close();
			} catch (IOException e) {
				System.out.println("ISSUE FROM writeHeader>>>");
			}

		}
	}

}

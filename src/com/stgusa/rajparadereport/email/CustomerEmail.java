package com.stgusa.rajparadereport.email;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;



public class CustomerEmail {

	private static final String FROM = "report@mf5tech.com";
	private static final String SUBJECT = "Company Reporting System";
	String mailServer = "172.16.21.220";
	String text;
	String prefix;
	String openFile;
	String shippedFile;
	boolean debug = true;
	String[] cc = { "boss@mf5tech.com", "secretary@mf5tech.com",
			"rep@mf5tech.com" };
	HashMap<String, String[]> customerHash = new HashMap<>();

	public CustomerEmail(String customerNumber, String[] emails, String prefix) {

		this.prefix = prefix;
		this.openFile = prefix + customerNumber + "/open.csv";
		this.shippedFile = prefix + customerNumber + "/shipped.csv";
		this.text = String
				.format("This is a report automatically generated for your"
						+ " convenience.\nPlease see attachement for customer: %s",
						customerNumber);
		
		//This file is always over 10MB.. Compress it
		if (customerNumber.matches("2720")) {
			shippedFile = prefix + customerNumber + "/shipped.csv.gz";

		}

		customerHash.put(customerNumber, emails);
	}

	public void setMailServer(String mailServerIP) {
		this.mailServer = mailServerIP;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void sendMail() {
		System.out.println(String.format(
				"Starting email with variables: mailServer: %s\nOpenFile: %s\n"
						+ "ShippedFile: %s\nText: %s\nFrom:%s\nSubject: %s\n",
				mailServer, openFile, shippedFile, text, FROM, SUBJECT));
		for (String[] list : customerHash.values()) {
			for (String email : list) {
				System.out.println("Emails are: " + email);
			}
		}
		
		for(String copies : cc){
			System.out.println(String.format("Copies are: %s\n", copies));
		}

		Properties props = System.getProperties();
		props.put("mail.smtp.host", mailServer);

		Session session = Session.getInstance(props, null);
		session.setDebug(debug);

		MimeMessage msg = new MimeMessage(session);

		// TO FROM TEXT SUBJECT RECIPIENTS
		try {
			msg.setFrom(new InternetAddress(FROM));
			msg.setSubject(SUBJECT);

			// Add administrative staff to all e-mails

			for (String[] list : customerHash.values()) {
				for (int i = 0; i < list.length; i++) {
					if (i == 0) {
						msg.addRecipients(Message.RecipientType.TO, list[i]);
					} else {
						msg.addRecipients(Message.RecipientType.CC, list[i]);
					}
				}
			}
			
			for (String copy : cc) {
				msg.addRecipients(Message.RecipientType.CC, copy);
			}

			MimeBodyPart mbp1 = new MimeBodyPart();
			mbp1.attachFile(openFile);

			MimeBodyPart mbp2 = new MimeBodyPart();
			mbp2.attachFile(shippedFile);

			MimeBodyPart mbp3 = new MimeBodyPart();
			mbp3.setText(text);

			Multipart mp = new MimeMultipart();
			mp.addBodyPart(mbp1);
			mp.addBodyPart(mbp2);
			mp.addBodyPart(mbp3);

			msg.setContent(mp);
			msg.setSentDate(new Date());

			Transport.send(msg);

		} catch (MessagingException | IOException e) {
			System.out.println("Error from sendMail()");
			System.out.println(e.getMessage());
		}

	}

//	public static void main(String[] args) {
//		DBQuery dbq = new DBQuery();
//		String[] emails = { "mintzmafia5@gmail.com",
//				"rbmintz.linuxadm@gmail.com" };
//		CustomerEmail ce = new CustomerEmail("2720", emails, dbq.getPrefix());
//		ce.sendMail();
//		System.out.println("DONE>>>");
//	}
}

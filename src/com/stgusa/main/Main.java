/**
 * author: Raymond Mintz, database view: Jeff Nikodym, Felix
 
 * 
 * date 10-31-2014 
 * 
 * Purpose:
 * 
 * 
 *
 * This will only email 2720, 2150, 1578, and 
 */
package com.stgusa.main;

import com.stgusa.rajparadereport.email.CustomerEmail;

public class Main {

	public static void main(String[] args) throws InterruptedException {

		DBQuery dbq = new DBQuery();
		dbq.Connection();

		String test = "rbmintz.linuxadm@gmail.com";
		String[] email2720 = {  "faddam@mf5tech.com",
				"dmullen@mf5tech.com", "hgrovenstein@mf5tech.com",test }; /* test
																	 will
																	 be
																	 taken
																	 out
																	 deleted
																	 after
																	 testing
																	 period */
		
		//add customer user email addresses to the ArrayList according to the custumer code.
		String[] email2150 = { "maribelf@mf5tech.com" };
		String[] email2703 = { "promero@mf5tech.com",
				"dmullen@mf5tech.com", "hgrovenstein@mf5tech.com",test };
		String[] email1578 = { "nthomas@dynamicbrands.com",
				"promero@mf5tech.com" };
		String[] email2243 = { "promero@mf5tech.com" };
		String[] email2714 = { "acastanon@mf5tech.com" };
		String[] email2687 = { "promero@mf5tech.com" };

		Compress comp = new Compress("/mnt/ftproot/2720/shipped.csv",
				"/mnt/ftproot/2720/shipped.csv.gz");
		comp.compressIt();

		CustomerEmail ce1578 = new CustomerEmail("1578", email1578,
				dbq.getPrefix());
		ce1578.sendMail();
		Thread.sleep(100);

		CustomerEmail ce2703 = new CustomerEmail("2703", email2703,
				dbq.getPrefix());
		ce2703.sendMail();
		Thread.sleep(100);

		CustomerEmail ce2150 = new CustomerEmail("2150", email2150,
				dbq.getPrefix());
		ce2150.sendMail();
		Thread.sleep(100);

		CustomerEmail ce2720 = new CustomerEmail("2720", email2720,
				dbq.getPrefix());
		ce2720.sendMail();
		Thread.sleep(100);

		CustomerEmail ce2243 = new CustomerEmail("2243", email2243,
				dbq.getPrefix());
		ce2243.sendMail();
		Thread.sleep(100);

		CustomerEmail ce2714 = new CustomerEmail("2714", email2714,
				dbq.getPrefix());
		ce2714.sendMail();
		Thread.sleep(100);

		CustomerEmail ce2687 = new CustomerEmail("2687", email2687,
				dbq.getPrefix());
		ce2687.sendMail();

	}
}
